package hello.upload.controller;

import hello.upload.domain.Item;
import hello.upload.domain.ItemRepository;
import hello.upload.domain.UploadFile;
import hello.upload.file.FileStore;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Slf4j
@Controller
@RequiredArgsConstructor
public class ItemController {

    private final ItemRepository itemRepository;
    private final FileStore fileStore;

    // 파일 업로드를 위한 폼 페이지 렌더링
    @GetMapping("/items/new")
    public String newItem(@ModelAttribute ItemForm form) {
        return "/item-form";
    }

    // 파일을 서버에 업로드하고 Item 을 데이터베이스에 저장
    @PostMapping("/items/new")
    public String saveItem(@ModelAttribute ItemForm form, RedirectAttributes redirectAttributes) throws IOException {
        // store attachFile to file.dir
        UploadFile attachFile = fileStore.storeFile(form.getAttachFile());

        // store imageFiles to file.dir
        List<UploadFile> storeImageFiles = fileStore.storeFiles(form.getImageFiles());

        // 데이터베이스에 저장
        Item item = new Item();
        item.setItemName(form.getItemName());
        item.setAttachFile(attachFile);
        item.setImageFiles(storeImageFiles);
        itemRepository.save(item);

        redirectAttributes.addAttribute("itemId", item.getId());

        return "redirect:/items/{itemId}";
    }

    // Item 상세 페이지 렌더링
    @GetMapping("/items/{id}")
    public String items(@PathVariable Long id, Model model) {
        Item item = itemRepository.findById(id);
        model.addAttribute("item", item);
        return "/item-view";
    }

    // 이미지를 화면에 표현
    @ResponseBody
    @GetMapping("/images/{filename}")
    public Resource downloadImage(@PathVariable String filename) throws MalformedURLException {
        return new UrlResource("file:" + fileStore.getFullPath(filename));
    }

    // 첨부파일 다운로드 (화면에 나타내는 것이 아니라 다운로드 수행)
    @GetMapping("/attach/{itemId}")
    public ResponseEntity<Resource> downloadAttach(@PathVariable Long itemId) throws MalformedURLException {
        // itemId 가지고 Item 얻고, Item 에서 서버 경로와 업로드 시 지정했떤 파일명을 담는 객체(attachFile) 획득
        Item item = itemRepository.findById(itemId);
        UploadFile attachFile = item.getAttachFile();

        // 서버에 저장된 경로와 파일명(storeFileName, UUID) 사용해서 파일 객체(UrlResource) 획득
        String storeFileName = attachFile.getStoreFileName();
        UrlResource resource = new UrlResource("file:" + fileStore.getFullPath(storeFileName));

        // 사용자가 업로드 시 지정했던 파일명 획득
        String uploadFileName = attachFile.getUploadFileName();
        log.info("uploadFileName={}", uploadFileName);

        // 파일명에 한글 또는 특수문자 있을 경우 대비하여 UTF-8 로 인코딩
        String encodedUploadFileName = UriUtils.encode(uploadFileName, StandardCharsets.UTF_8);

        // 파일을 열지 않고 다운로드 받도록 하기 위해서는 Content-Disposition 헤더 값을 아래와 같이 지정해야 함
        String contentDisposition = "attachment; filename=\"" + encodedUploadFileName + "\"";

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, contentDisposition)
                .body(resource);
    }

}
