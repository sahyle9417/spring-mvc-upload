package hello.upload.file;

import hello.upload.domain.UploadFile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class FileStore {

    @Value("${file.dir}")
    private String fileDir;

    public String getFullPath(String filename) {
        return fileDir + filename;
    }

    // 파일 여러개 저장
    public List<UploadFile> storeFiles(List<MultipartFile> multipartFiles) throws IOException {
        List<UploadFile> storeFileResult = new ArrayList<>();
        for (MultipartFile multipartFile : multipartFiles) {
            if (!multipartFile.isEmpty()) {
                storeFileResult.add(storeFile(multipartFile));
            }
        }
        return storeFileResult;
    }

    // 단일 파일 저장
    public UploadFile storeFile(MultipartFile multipartFile) throws IOException {
        if (multipartFile.isEmpty()) {
            return null;
        }

        // originalFilename : 원본 파일명
        String originalFilename = multipartFile.getOriginalFilename();
        // storeFileName : 서버에 저장할 파일명, UUID.기존확장자
        String storeFileName = createStoreFileName(originalFilename);
        // 입력받은 파일 객체(MultipartFile)를 실제 서버 경로에 저장
        multipartFile.transferTo(new File(getFullPath(storeFileName)));

        // 원본 파일과 서버에 저장된 파일 경로 정보 담는 객체 반환
        return new UploadFile(originalFilename, storeFileName);
    }

    // 파일명을 UUID 로 변경 (서로 다른 사용자의 파일이 덮어쓰기 되는 것을 방지)
    private String createStoreFileName(String originalFilename) {
        String ext = extractExt(originalFilename);
        String uuid = UUID.randomUUID().toString();
        return uuid + "." + ext;
    }

    // 파일명에서 확장자 추출
    private String extractExt(String originalFilename) {
        int pos = originalFilename.lastIndexOf(".");
        return originalFilename.substring(pos + 1);
    }

}
